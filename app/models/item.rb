class Item < ActiveRecord::Base
  has_many :bids
  belongs_to :user

  # default_scope where('available_on < Time.now')


  # scope :new, order("created_at ASC")
  scope :active, -> { where('available_on < ?', Time.now)}
  scope :latest, -> { where('available_on < ?', Time.now).order("created_at ASC")}
  scope :expired, -> { where('closing_on < ?', Time.now)}
  # scope :expired, where("closing_on < Time.now" ).order("created_at ASC")
end
