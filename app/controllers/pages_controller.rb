class PagesController < ApplicationController
  def faq
  end

  def contact
  end

  def legal
  end

  def terms
  end
end
