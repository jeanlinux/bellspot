class VisitorsController < ApplicationController
  def index
    @featured_items = Item.active.limit(4)
    @latest_items = Item.latest.limit(10)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: {featured_items: featured_items, latest_items: latest_items} }
    end
  end
end
