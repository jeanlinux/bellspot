Rails.application.routes.draw do

  get 'faq' => 'pages#faq'

  get 'contact_us' => 'pages#contact'

  get 'legal' => 'pages#legal'

  get 'special_offers' => 'items#special'

  get 'terms_and_conditions' => 'pages#terms'

  # get 'items/index'
  #
  # get 'items/:id' => "items#show"
 resources :items

  ActiveAdmin.routes(self)
  root to: 'visitors#index'
  devise_for :users
  resources :users
end
