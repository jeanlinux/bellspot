# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = CreateAdminService.new.call
puts 'CREATED ADMIN USER: ' << user.email
# Environment variables (ENV['...']) can be set in the file config/application.yml.
# See http://railsapps.github.io/rails-environment-variables.html


# Seed the database with sample items

1.upto 12 do |n|
  Item.create({name: "Sample Item", tagline: "Here is our cool item", description: "Here is our sample item, and we hope you like it, go ahead and bid on it", available_on: Time.now(), closing_on: Time.current.advance(weeks: 4), reserved_price: 20000, user_id:User.first.id })
end
puts "Created sample items : #{ Item.active.count}"

#create expired listings
1.upto 9 do |n|
  Item.create({name: "Sample Item 2", description: "Here is our sample item, and we hope you like it, go ahead and bid on it", available_on: 2.weeks.ago, closing_on: Time.now, reserved_price: 20000, user_id:User.first.id })
end
puts "Created expired items : #{ Item.expired.count}"