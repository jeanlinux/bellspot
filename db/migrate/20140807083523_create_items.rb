class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.text :description
      t.datetime :available_on
      t.datetime :closing_on
      t.float :reserved_price
      t.datetime :deleted_at
      t.string :permalink
      t.text :meta_description
      t.string :meta_keywords
      t.integer :user_id

      t.timestamps
    end
  end
end
