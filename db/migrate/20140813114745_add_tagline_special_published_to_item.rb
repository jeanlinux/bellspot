class AddTaglineSpecialPublishedToItem < ActiveRecord::Migration
  def change
    add_column :items, :tagline, :text
    add_column :items, :special, :boolean
    add_column :items, :published, :boolean
  end
end
