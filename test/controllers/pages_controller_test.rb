require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get faq" do
    get :faq
    assert_response :success
  end

  test "should get contact" do
    get :contact
    assert_response :success
  end

  test "should get legal" do
    get :legal
    assert_response :success
  end

  test "should get terms" do
    get :terms
    assert_response :success
  end

end
